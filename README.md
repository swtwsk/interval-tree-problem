# Interval tree problem #

Given the problem: keep disjoint intervals of numbers and update them (add new intervals, join intersecting, remove intervals) in log2 time and return number of all the elements in all of the intervals, I searched for an answer.

At first, I made a solution based on my OCaml project written earlier, which was a modification of AVL tree.
It does work, but unfortunately there is a problem with memory leaks (OCaml has a garbage collector while C++ does not).
I had not put required effort to patch them, so it is a buggy implementation, but nevertheless I am proud of it. _Maybe_ some day I will get back to it and repair it.

Then I have written my implementation of segment tree with lazy propagation.

Operations provided by this implementation:  
 * Insert (assignment) on given interval  
 * Query on whole tree (because I needn't to implement full query operation for this task)
