#include <iostream>
#include <algorithm>
#include <utility>

using std::cin;
using std::cout;
using std::endl;

using Interval = std::pair<int, int>;

struct Node;

inline int split_cmp(int a, int b, int x) {
    return x <= b ? (x >= a ? 0 : -1) : 1;
}

inline int amount(Interval& interval) {
    return interval.second - interval.first + 1;
}

struct Node {
    Node* left_son;
    Interval interval;
    Node* right_son;
    int height;
    int elements;

    Node() : left_son(nullptr), interval(Interval()), right_son(nullptr), height(0), elements(0) {};

    Node(Interval& interval) : 
        left_son(nullptr), interval(interval), right_son(nullptr), height(1), elements(amount(interval)) {};

    Node(Node* left_son, Interval& interval, Node* right_son) 
        : left_son(left_son), interval(interval), right_son(right_son) {
        //height
        int left_height = (left_son == nullptr ? 0 : left_son->height);
        int right_height = (right_son == nullptr ? 0 : right_son->height);
        height = std::max(left_height, right_height) + 1;

        int left_elements = (left_son == nullptr ? 0 : left_son->elements);
        int right_elements = (right_son == nullptr ? 0 : right_son->elements);
        elements = left_elements + right_elements + amount(interval);
    }

    bool is_empty() {
        return elements == 0;
    }
};

inline int height(Node* n) {
    return n == nullptr ? 0 : n->height;
}
inline int elements(Node* n) {
    return n == nullptr ? 0 : n->elements;
}

Node* make(Node* left_son, Interval& interval, Node* right_son) {
    return new Node(left_son, interval, right_son);
}

Node* balance(Node* left_son, Interval& interval, Node* right_son) {
    int hl = height(left_son);
    int hr = height(right_son);

    Node* balanced = nullptr;

    if (hl > hr + 2) {
        if (height(left_son->left_son) >= height(left_son->right_son)) {
            balanced = make(left_son->left_son, left_son->interval, 
                make(left_son->right_son, interval, right_son));
            //delete left_son; //chyba xD
        }
        else {
            balanced = make(make(left_son->left_son, left_son->interval, left_son->right_son->left_son),
                left_son->right_son->interval, make(left_son->right_son->right_son, interval, right_son));
            //delete left_son->right_son;
            //delete left_son;
        }
    }
    else if (hr > hl + 2) {
        if (height(right_son->right_son) >= height(right_son->left_son)) {
            balanced = make(make(left_son, interval, right_son->left_son),
                right_son->interval, right_son->right_son);
            //delete right_son;
        }
        else {
            balanced = make(make(left_son, interval, right_son->left_son->left_son),
                right_son->left_son->interval, make(right_son->left_son->right_son,
                    right_son->interval, right_son->right_son));
            //delete right_son->left_son;
            //delete right_son;
        }
    }
    else {
        balanced = make(left_son, interval, right_son);
    }

    return balanced;
}

struct Merge {
    Interval left_interval;
    Interval right_interval;
    bool left;
    bool right;

    Merge(Interval& left_interval, Interval& right_interval, bool left, bool right) :
        left_interval(left_interval), right_interval(right_interval), left(left), right(right) {};
};

inline bool cmp(int x, int y) {
    return 0 <= std::abs(x - y) && std::abs(x - y) <= 1;
}
Merge int_merge(Interval& left_interval, Interval& right_interval) {
    using std::make_pair;

    Interval null_interval = std::make_pair(-1, -1);

    int a = left_interval.first;
    int b = left_interval.second;
    int c = right_interval.first;
    int d = right_interval.second;

    if (a <= c) {
        if (b <= c) {
            if (cmp(b, c)) {
                Interval i = make_pair(a, d);
                return Merge(null_interval, i, true, false);
            }
            else {
                return Merge(left_interval, right_interval, true, false);
            }
        }
        else {
            if (b <= d) {
                Interval i = make_pair(a, d);
                return Merge(null_interval, i, true, false);
            }
            else {
                return Merge(null_interval, left_interval, true, true);
            }
        }
    }
    else {
        if (d <= a) {
            if (cmp(d, a)) {
                Interval i = make_pair(c, b);
                return Merge(null_interval, i, false, true);
            }
            else {
                return Merge(right_interval, left_interval, false, true);
            }
        }
        else {
            if (d <= b) {
                Interval i = make_pair(c, b);
                return Merge(null_interval, i, false, true);
            }
            else {
                return Merge(null_interval, right_interval, false, false);
            }
        }
    }
}

struct Folded {
    Node* new_subtree;
    int x;

    Folded(Node* new_subtree, int x) : new_subtree(new_subtree), x(x) {};
};
Folded fold_left(int x, Node* tree) {
    if (tree == nullptr || tree->is_empty()) {
        return Folded(nullptr, x);
    }
    else {
        if (x < tree->interval.first) {
            return fold_left(x, tree->left_son);
        }
        else if (tree->interval.second < x) {
            Folded folded = fold_left(x, tree->right_son);
            Folded to_return = Folded(make(tree->left_son, tree->interval, folded.new_subtree), folded.x);
            delete tree;
            return to_return;
        }
        else {
            return Folded(tree->left_son, tree->interval.first);
        }
    }
}
Folded fold_right(int y, Node* tree) {
    if (tree == nullptr || tree->is_empty()) {
        return Folded(nullptr, y);
    }
    else {
        if (tree->interval.second < y) {
            return fold_right(y, tree->right_son);
        }
        else if (y < tree->interval.first) {
            Folded folded = fold_right(y, tree->left_son);
            Folded to_return = Folded(make(folded.new_subtree, tree->interval, tree->right_son), folded.x);
            delete tree;
            return to_return;
        }
        else {
            return Folded(tree->right_son, tree->interval.second);
        }
    }
}

bool is_nullint(Interval& i) {
    return i.first == -1 && i.second == -1;
}
Node* add_one(Interval& i, Node* tree) {
    using std::make_pair;

    Node* to_return;

    if (tree == nullptr || tree->is_empty()) {
        delete tree;
        to_return = new Node(i);
        return to_return;
    }

    int lk = tree->interval.first;
    int rk = tree->interval.second;
    int x = i.first;
    int y = i.second;

    if (rk < x) {
        Node* to_add = add_one(i, tree->right_son);
        to_return = balance(tree->left_son, tree->interval, to_add);
    }
    else if (y < lk) {
        Node* to_add = add_one(i, tree->left_son);
        to_return = balance(to_add, tree->interval, tree->right_son);
    }
    else {
        Merge m = int_merge(i, tree->interval);

        if (is_nullint(m.left_interval)) {
            if (m.left) {
                if (m.right) {
                    Folded l = fold_left(x, tree->left_son);
                    Folded r = fold_right(y, tree->right_son);
                    Interval i = make_pair(l.x, r.x);
                    to_return = balance(l.new_subtree, i, r.new_subtree);
                }
                else {
                    Folded l = fold_left(x, tree->left_son);
                    Interval i = make_pair(l.x, m.right_interval.second);
                    to_return = balance(l.new_subtree, i, tree->right_son);
                }
            }
            else {
                if (m.right) {
                    Folded r = fold_right(y, tree->right_son);
                    Interval i = make_pair(m.right_interval.first, r.x);
                    to_return = balance(tree->left_son, i, r.new_subtree);
                }
                else {
                    to_return = balance(tree->left_son, m.right_interval, tree->right_son);
                }
            }

        }
        else {
            if (m.left) {
                to_return = balance(add_one(m.left_interval, tree->left_son), m.right_interval, tree->right_son);
            }
            else {
                to_return = balance(tree->left_son, m.left_interval, add_one(m.right_interval, tree->right_son));
            }
        }
    }
    //delete tree;
    return to_return;
}

Node* join(Node* left, Interval& v, Node* right) {
    if (left == nullptr || left->is_empty()) {
        return add_one(v, right);
    }
    else if (right == nullptr || right->is_empty()) {
        return add_one(v, left);
    }
    else {
        if (height(left) > height(right) + 2) {
            return balance(left->left_son, left->interval, join(left->right_son, v, right));
        }
        else if (height(right) > height(left) + 2) {
            return balance(join(left, v, right->left_son), right->interval, right->right_son);
        }
        else {
            return make(left, v, right);
        }
    }
}

struct Split {
    Node* l;
    bool present;
    Node* r;

    Split(Node* l, bool present, Node* r) : l(l), present(present), r(r) {};
};
Split split_loop(int x, Node* s) {
    using std::make_pair;

    if (s == nullptr || s->is_empty()) {
        return Split(nullptr, false, nullptr);
    }

    int lk = s->interval.first;
    int rk = s->interval.second;
    int c = split_cmp(lk, rk, x);

    if (c == 0) {
        if (lk == x) {
            if (rk == x) {
                Split ns = Split(s->left_son, true, s->right_son);
                return ns;
            }
            else {
                Interval i = make_pair(x + 1, rk);
                Node* new_right = add_one(i, s->right_son);
                Split ns = Split(s->left_son, true, new_right);
                return ns;
            }
        }
        else {
            if (rk == x) {
                Interval i = make_pair(lk, x - 1);
                Node* new_left = add_one(i, s->left_son);
                Split ns = Split(new_left, true, s->right_son);
                return ns;
            }
            else {
                Interval i = make_pair(lk, x - 1);
                Interval j = make_pair(x + 1, rk);

                Node* left = add_one(i, s->left_son);
                Node* right = add_one(j, s->right_son);
                Split ns = Split(left, true, right);
                return ns;
            }
        }
    }
    else if (c < 0) {
        Split after_loop = split_loop(x, s->left_son);
        Node* new_right = join(after_loop.r, s->interval, s->right_son);
        return Split(after_loop.l, after_loop.present, new_right);
    }
    else {
        Split after_loop = split_loop(x, s->right_son);
        Node* new_left = join(s->left_son, s->interval, after_loop.l);
        return Split(new_left, after_loop.present, after_loop.r);
    }
}
Split split(int x, Node *s) {
    return split_loop(x, s);
}

Node* remove_loop(Node* l, Node* r) {
    if (l == nullptr || l->is_empty()) {
        return r;
    }
    else if (r == nullptr || r->is_empty()) {
        return l;
    }
    else {
        Node* balanced;
        if (height(l) > height(r) + 2) {
            balanced = balance(l->left_son, l->interval, remove_loop(l->right_son, r));
            delete l;
        }
        else {
            balanced = balance(remove_loop(l, r->left_son), r->interval, r->right_son);
            delete r;
        }
        return balanced;
    }
}

void delete_tree(Node* tree) {
    if (tree == nullptr) {
        return;
    }
    if (tree->left_son != nullptr) {
        delete_tree(tree->left_son);
    }
    if (tree->right_son != nullptr) {
        delete_tree(tree->right_son);
    }
    delete tree;
}

Node* remove(Interval& i, Node* s) {
    Split left_split = split(i.first, s);
    Node* left = left_split.l;

    Split right_split = split(i.second, s);
    Node* right = right_split.r;

    delete left_split.r;
    delete right_split.l;
    
    delete s;
    Node* after_loop = remove_loop(left, right);
    return after_loop;
}

Node* add(Interval& i, Node* s) {
    Node* n = remove(i, s);
    Node* to_return = add_one(i, n);
    return to_return;
}

/*int main() {
    int n;
    cin >> n;
    int m;
    cin >> m;

    Node* tree = new Node();

    for (int i = 0; i < m; i++) {
        int a, b;
        char c;
        cin >> a >> b >> c;

        Interval in = std::make_pair(a, b);
        if (c == 'C') {
            Node* new_tree = remove(in, tree);
            tree = new_tree;
        }
        if (c == 'B') {
            Node* new_tree = add(in, tree);
            tree = new_tree;
        }

        cout << elements(tree) << endl;
    }

    delete_tree(tree);
}*/