#include <iostream>
#include <algorithm>
#include <cmath>

using std::cin;
using std::cout;
using std::endl;

#define MAXN 2097153
#define M 1048575

int w[MAXN]; // value in the node
int W[MAXN]; // sum in the interval
bool actual[MAXN];

int left[MAXN];
int right[MAXN];

inline int left_son(int x) {
    return x > M ? 0 : 2 * x;
}
inline int right_son(int x) {
    return x > M ? 0 : 2 * x + 1;
}
inline int width(int x) {
    return right[x] - left[x] + 1;
}

/* Pre-processing to set intervals in every verticle */
void setIntervals(int x, int l, int r) {
    left[x] = l;
    right[x] = r;

    int szer = width(x);

    if (x <= M)
    {
        setIntervals(left_son(x), l, l + szer / 2 - 1);
        setIntervals(right_son(x), l + szer / 2, r);
    }
}

// assuming a <= b and x <= y
inline bool condition(int a, int b, int x, int y) {
    return (x >= a && y <= b) || (y < a || x > b);
}

void push_down(int a, int b, int vert) {
    if (condition(a, b, left[vert], right[vert])) {
        return;
    }

    if (actual[vert]) {
        w[left_son(vert)] = w[vert];
        actual[left_son(vert)] = true;
        w[right_son(vert)] = w[vert];
        actual[right_son(vert)] = true;

        actual[vert] = false;
        w[vert] = 0;
    }
}

inline void updateW(int node) {
    W[node] = actual[node] ? width(node) * w[node] : W[left_son(node)] + W[right_son(node)];
}

void insert(int a, int b, int val, int node) {
    push_down(a, b, node);
    
    if (left[node] == a && right[node] == b)
    {
        w[node] = val;
        actual[node] = true;
    }
    else if (a <= b && a <= right[node] && b >= left[node])
    {
        int lewy = left_son(node);
        int prawy = right_son(node);

        int prawy_lewy = right[lewy];
        insert(a, (b >= prawy_lewy ? prawy_lewy : b), val, lewy);
        int lewy_prawy = left[prawy];
        insert((a <= lewy_prawy ? lewy_prawy : a), b, val, prawy);
    }

    updateW(node);
}

int main() {
    setIntervals(1, 1, M + 1);
    
    int n;
    cin >> n;
    int m;
    cin >> m;

    for(int i = 0; i < m; i++) {
        int a, b;
        char c;
        cin >> a >> b >> c;
        
        insert(a, b, (c == 'B' ? 1 : 0), 1);

        cout << W[1] << endl; // returns sum on the whole tree
    }
}